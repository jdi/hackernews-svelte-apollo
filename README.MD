# Svelte + Apollo + Graphcool Hackernews-like application

This was made following along the same line of tutorials from https://www.howtographql.com/

The GraphQL Schema is the same used in both their React-Relay and React-Apollo tutorials, which you can find here: https://graphqlbin.com/hn-starter.graphql

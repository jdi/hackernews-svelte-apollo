import ApolloClient, { createNetworkInterface } from 'apollo-client'
import { GRAPHCOOL_API_LINK } from '../../api.js'
import { GC_AUTH_TOKEN } from '../constants'

const networkInterface = createNetworkInterface({
  uri: GRAPHCOOL_API_LINK
})

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {}
    }
    const token = localStorage.getItem(GC_AUTH_TOKEN)
    req.options.headers.authorization = token ? `Bearer ${token}` : null
    next()
  }
}])

const client = new ApolloClient({
  networkInterface
});

export default client

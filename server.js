const express = require('express')
const app = express()
var path = require('path')

app.use(express.static(path.join(__dirname, 'public')))

app.get(['/', '/create'], function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'))
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
